# Collection - Marcelo Silva
---
![](https://avatars3.githubusercontent.com/u/43017609?s=280&v=4)
### Time Complexity
---
|           | ArrayList | LinkedList |   HashSet |  HashMap  |  TreeSet  |  TreeMap  |
| --------- | --------- | ---------  | --------- | --------- | --------- | --------- |
| Insertion | O(1)      | O(1)       | O(n)      | O(n)      | O(log(n)) | O(log(n)) |
| Search    | O(1)      | O(n)       | O(n)      | O(n)      | O(log(n)) | O(log(n)) |
| Deletion  | O(n)      | O(n)       | O(n)      | O(n)      |           |           |



# Installation
---
Cloning , setup and testing

```
$ git clone https://MarceloSilva_@bitbucket.org/MarceloSilva_/collections.git
$ cd collections/
$ mvn clean package
$ open target/site/jacoco/index.html
```

# Built With
---
* [Java](https://docs.oracle.com/javase/8/docs/) - The development language
* [Maven](https://maven.apache.org/) - Dependency Management