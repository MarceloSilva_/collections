package com.marcelo.arrayList;

import java.util.*;

public class ArrayList<E> implements List<E> {
    private static final int INITIAL_ARRAY_LENGTH = 10;
    private int size;
    private E[] obj;


    public ArrayList() {
        obj = (E[]) new Object[INITIAL_ARRAY_LENGTH];
    }

    // Returns the number of obj in this list.
    public int size() {
        return size;
    }

    // Returns true if this list contains no obj.
    public boolean isEmpty() {
        return size == 0;
    }

    // Appends the specified element to the end of this list.
    public boolean add(E e) {
        if (size == obj.length) {
            increaseCapacity();
        }
        obj[size] = e;
        size++;
        return true;
    }

    // Increases the capacity of the Array
    private void increaseCapacity() {
        obj = Arrays.copyOf(obj, obj.length * 2);
    }

    // Returns the element at the specified position in this list.
    public E get(int index) {
        if (isOutOfBounds(index)) {
            throw new IndexOutOfBoundsException();
        }
        return obj[index];
    }


    public Object[] toArray() {
        return (E[]) Arrays.copyOf(obj, size);
    }

    // Removes all of the obj from this list. The list will be empty after this call returns.
    public void clear() {
        obj = (E[]) new Object[INITIAL_ARRAY_LENGTH];
        size = 0;
    }

    public E remove(int index) {
        if (isOutOfBounds(index)) {
            throw new IndexOutOfBoundsException();
        }
        E removed = obj[index];
        E[] temp = (E[]) new Object[obj.length];
        for (int i = 0, j = 0; i < obj.length; i++) {
            if (i != index) {
                temp[j++] = obj[i];
            }
        }
        size--;
        obj = temp;
        return removed;
    }

    /*
     * Returns the index of the first occurrence of the specified element in this list, or -1 if this list does not contain the element.
     * More formally, returns the lowest index i such that (o==null ? get(i)==null : o.equals(get(i))), or -1 if there is no such index.
     */
    public int indexOf(Object o) {
        if (o == null) {
            for (int i = 0; i < obj.length; i++) {
                if (obj[i] == null) {
                    return i;
                }
            }
        } else {
            for (int i = 0; i < obj.length; i++) {
                if (o.equals(obj[i])) {
                    return i;
                }
            }
        }
        return -1;
    }

    /*
     *  Returns the index of the last occurrence of the specified element in this list, or -1 if this list does not contain the element.
     *  More formally, returns the highest index i such that (o==null ? get(i)==null : o.equals(get(i))), or -1 if there is no such index.
     */
    public int lastIndexOf(Object o) {
        if (o == null) {
            for (int i = obj.length - 1; i >= 0; i--) {
                if (obj[i] == null) {
                    return i;
                }
            }
        } else {
            for (int i = obj.length - 1; i >= 0; i--) {
                if (o.equals(obj[i])) {
                    return i;
                }
            }
        }
        return -1;
    }

    public boolean contains(Object o) {
        if (o == null) {
            for (int i = 0; i < obj.length; i++) {
                if (obj[i] == null) {
                    return true;
                }
            }
        } else {
            for (int i = 0; i < obj.length; i++) {
                if (o.equals(obj[i])) {
                    return true;
                }
            }
        }
        return false;
    }

    public boolean remove(Object o) {
        for (int i = 0; i < size; i++) {
            if (o.equals(get(i))) {
                int j = i;
                for (; j < size; j++) {
                    obj[j] = obj[j + 1];
                }
                obj[j + 1] = null;
                size--;
                return true;
            }
        }
        return false;
    }

    public void add(int index, E element) {
        if (index < 0 || index > size()) {
            throw new IndexOutOfBoundsException();
        }
        if (size + 1 == obj.length) {
            increaseCapacity();
        }
        for (int i = size; i >= index; i--) {
            obj[i + 1] = obj[i];
        }
        obj[index] = (E) element;
        size++;
    }

    public E set(int index, Object element) {
        if (index < 0 || index >= size()) {
            throw new IndexOutOfBoundsException();
        }
        E prev = obj[index];
        obj[index] = (E) element;
        return prev;
    }

    private boolean isOutOfBounds(int index) {
        return index >= size() || index < 0;
    }

    public boolean addAll(Collection<? extends E> c) {
        if (c == null) {
            throw new NullPointerException();
        }
        for (E e : c) {
            add(e);
        }
        return true;
    }

    public boolean removeAll(Collection<?> c) {
        for (Object o : c.toArray()) {
            remove(o);
        }
        return true;
    }

    public boolean containsAll(Collection<?> c) {
        int i = 0;
        for (Object o : c) {
            if (contains(o)) {
                i++;
            }
        }
        if (i == c.size()) {
            return true;
        }
        return false;
    }

    public List subList(int fromIndex, int toIndex) {
        List<E> list = new ArrayList<E>();
        if (fromIndex < 0 || toIndex > size) {
            throw new IndexOutOfBoundsException();
        }
        if (fromIndex > toIndex) {
            throw new IllegalArgumentException();
        }
        for (int i = fromIndex; i < toIndex; i++) {
            list.add(obj[i]);
        }
        return list;
    }

    public boolean addAll(int index, Collection<? extends E> c) {
        if(c==null){
            throw new NullPointerException();
        }
        if(index < 0 || index > size()){
            throw new IndexOutOfBoundsException();
        }

        int i = index;
        for (Object o : c) {
            add(i,(E)o);
            i++;
        }
        return true;
    }

    public Iterator iterator() {
        Iterator<Object> it = new Iterator<Object>() {
            private int currentIndex = 0;

            @Override
            public boolean hasNext() {
                return currentIndex < size;
            }

            @Override
            public Object next() {
                return obj[currentIndex++];
            }
        };

        return it;
    }

    // ####################TODO

    public Object[] toArray(Object[] a) {
        return new Object[0];
    }

    public boolean retainAll(Collection c) {
        return false;
    }

    public ListIterator listIterator() {
        return null;
    }

    public ListIterator listIterator(int index) {
        return null;
    }
}
