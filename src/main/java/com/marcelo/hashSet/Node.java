package com.marcelo.hashSet;

public class Node<K> {
    private final int hash;
    private K key;
    Node<K> next;

    public Node(int hash, K key, Node<K> next) {
        this.hash = hash;
        this.key = key;
        this.next = next;
    }

    public int getHash() {
        return hash;
    }

    public K getKey() {
        return key;
    }

    public Node<K> getNext() {
        return next;
    }

    public void setNext(Node<K> next) {
        this.next = next;
    }
}
