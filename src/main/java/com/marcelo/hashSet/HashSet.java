package com.marcelo.hashSet;


import java.util.*;

import static java.util.Objects.hash;

public class HashSet<K> implements Set<K> {
    private final int DEFAULT_BUCKETS = 16;
    private int n;
    private int size;
    private Node<K>[] buckets;


    public HashSet() {
        n = DEFAULT_BUCKETS;
        buckets = new Node[n];
    }

    public HashSet(int nBukets) {
        n = nBukets;
        buckets = new Node[n];
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public boolean isEmpty() {
        return size == 0;
    }

    @Override
    public boolean contains(Object o) {
        int hash = hash(o);
        int index = calculateIndex(hash);

        Node<K> current = buckets[index];
        if (current == null) {
            return false;
        } else {
            if (o == null) {  // Searching for null key
                if (current.getHash() == hash && current.getKey() == o) {
                    return true;
                }
                while (current.getNext() != null) {
                    current = current.getNext();
                    if (current.getHash() == hash && current.getKey() == o) {
                        return true;
                    }
                }
            } else {   //Searching for key different from null
                if (current.getHash() == hash && current.getKey().equals(o)) {
                    return true;
                }
                while (current.getNext() != null) {
                    current = current.getNext();
                    if (current.getHash() == hash && current.getKey().equals(o)) {
                        return true;
                    }
                }
            }
            return false;
        }
    }


    @Override
    public Iterator<K> iterator() {
        Iterator<K> it = new Iterator<K>() {
            private int currentBucket;
            private Node currentNode;

            @Override
            public boolean hasNext() {
                if (currentNode != null && currentNode.getNext() != null) {
                    return true;
                }

                for (int index = currentBucket + 1; index < buckets.length; index++) {
                    if (buckets[index] != null) {
                        return true;
                    }
                }
                return false;
            }

            @Override
            public K next() {
                if (currentNode == null || currentNode.next == null) {
                    currentBucket++;
                    while (currentBucket < buckets.length && buckets[currentBucket] == null) {
                        currentBucket++;
                    }
                    if (currentBucket < buckets.length) {
                        currentNode = buckets[currentBucket];
                    }
                } else {
                    currentNode = currentNode.next;
                }
                return (K) currentNode.getKey();

            }
        };
        return it;
    }

    @Override
    public Object[] toArray() {//TODO
        return new Object[0];
    }

    @Override
    public <T> T[] toArray(T[] a) {//TODO
        return null;
    }

    @Override
    public boolean add(K key) {
        int hash = hash(key);
        int index = calculateIndex(hash);

        Node<K> current = buckets[index];
        if (current == null) {
            buckets[index] = new Node<K>(hash, key, null);
            size++;
            //System.out.println("EMPTY :      Hash: " + hash + " Index: " + index + " RETURNIG: null");
            return true;
        } else {
            do {
                if (current.getHash() == hash) {
                    //System.out.println("REPETIDO");
                    return false;
                }
                if (current.getNext() != null) {
                    current = current.getNext();
                } else {
                    break;
                }
            } while (current != null);
            current.setNext(new Node(hash, key, null));
            size++;
            //System.out.println("NOT_EMPTY:   Hash: " + hash + " Index: " + index + " RETURNIG: " + current.getValue());
            return true;
        }
    }

    @Override
    public boolean remove(Object key) {
        int hash = hash(key);
        int index = calculateIndex(hash);

        Node<K> current = buckets[index];
        Node<K> prev = buckets[index];
        int counter = 0;
        if (current != null) {
            do {
                if (key != null) { //    Compare with .equal  - not null
                    if (key.equals(current.getKey())) {
                        if (counter == 0) {
                            buckets[index] = current.getNext();
                            size--;
                            return true;
                        } else {
                            prev.setNext(current.getNext());
                            size--;
                            return true;
                        }
                    }
                } else { // Compare with ==  - key == null
                    if (key == current.getKey()) {
                        if (counter == 0) {
                            buckets[index] = current.getNext();
                            size--;
                            return true;
                        } else {
                            prev.setNext(current.getNext());
                            size--;
                            return true;
                        }
                    }
                }
                prev = current;
                current = current.getNext();
                counter++;
            } while (current != null);
        }
        return false;
    }

    @Override
    public boolean containsAll(Collection<?> c) {
        for (Object o : c) {
            if (!contains(o)) {
                return false;
            }
        }
        return true;
    }

    @Override
    public boolean addAll(Collection<? extends K> c) {
        boolean changed = false;
        for (K key : c) {
            if (add(key)) {
                changed = true;
            }
        }
        return changed;
    }

    @Override
    public boolean retainAll(Collection<?> c) {//TODO
        return false;
    }

    @Override
    public boolean removeAll(Collection<?> c) {
        boolean changed = false;
        for (Object o : c) {
            if (remove(o)) {
                changed = true;
            }
        }
        return changed;
    }

    @Override
    public void clear() {
        size = 0;
        buckets = new Node[n];
    }

    private int calculateIndex(int hash) {
        return hash & (n - 1);
    }

/*  Print
    @Override
    public String toString() {

        Node currentNode = null;
        StringBuffer sb = new StringBuffer();

        // loop through the array
        for (int index = 0; index < buckets.length; index++) {
            // we have an entry
            if (buckets[index] != null) {
                currentNode = buckets[index];
                sb.append("[" + index + "]");
                if (currentNode.getKey() != null) {
                    sb.append(" " + currentNode.getKey().toString());
                } else {
                    sb.append(" null");
                }

                while (currentNode.getNext() != null) {
                    currentNode = currentNode.getNext();
                    if (currentNode.getKey() != null) {
                        sb.append(" -> " + currentNode.getKey().toString());
                    } else {
                        sb.append(" -> null");
                    }
                }
                sb.append('\n');
            }
        }

        return sb.toString();
    }*/

}
