package com.marcelo.treeSet;

public class Node<E> {
    private E element;
    private Node<E> right;
    private Node<E> left;

    public Node(E element) {
        this.element = element;
    }

    public Node<E> getRight() {
        return right;
    }

    public void setRight(Node<E> right) {
        this.right = right;
    }

    public Node<E> getLeft() {
        return left;
    }

    public void setLeft(Node<E> left) {
        this.left = left;
    }

    public E getElement() {
        return element;
    }
}
