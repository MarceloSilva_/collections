package com.marcelo.treeSet;

import java.util.*;

public class TreeSet<T> implements Set<T> {
    private int size;
    private Node<T> root;
    private Comparator<T> comparator;

    public TreeSet(Comparator<T> comparator) {
        this.comparator = comparator;
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public boolean isEmpty() {
        return size == 0;
    }

    @Override
    public boolean contains(Object o) {
        if (isEmpty()) {
            return false;
        }
        Node<T> currentNode = root;
        while (currentNode != null) {
            int comp = comparator.compare(currentNode.getElement(), (T) o);
            if (comp == 0) {
                return true;
            } else if (comp < 0) {
                if (currentNode.getRight() != null) {
                    currentNode = currentNode.getRight();
                } else {
                    return false;
                }
            } else if (comp > 0) {
                if (currentNode.getLeft() != null) {
                    currentNode = currentNode.getLeft();
                } else {
                    return false;
                }
            }
        }
        return false;
    }

    private Node<T>[] getNodes(Object o) {
        if (isEmpty()) {
            return null;
        }
        Node<T> currentNode = root;
        Node<T> prevNode = root;
        while (currentNode != null) {
            int comp = comparator.compare(currentNode.getElement(), (T) o);
            if (comp == 0) {
                return new Node[]{prevNode, currentNode};
            } else if (comp < 0) {
                if (currentNode.getRight() != null) {
                    prevNode = currentNode;
                    currentNode = currentNode.getRight();
                } else {
                    return null;
                }
            } else if (comp > 0) {
                if (currentNode.getLeft() != null) {
                    prevNode = currentNode;
                    currentNode = currentNode.getLeft();
                } else {
                    return null;
                }
            }
        }
        return null;
    }

    @Override
    public Iterator<T> iterator() {
        return null;
    }

    @Override
    public Object[] toArray() {
        return new Object[0];
    }

    @Override
    public <T> T[] toArray(T[] a) {
        return null;
    }

    @Override
    public boolean add(T e) {
        if (isEmpty()) {
            root = new Node<>(e);
            size++;
            return true;
        }
        Node<T> currentNode = root;
        if (comparator != null) {
            while (currentNode != null) {
                int comp = comparator.compare(currentNode.getElement(), e);
                if (comp == 0) {
                    return false;
                } else if (comp < 0) {
                    if (currentNode.getRight() != null) {
                        currentNode = currentNode.getRight();
                    } else {
                        currentNode.setRight(new Node<>(e));
                        size++;
                        return true;
                    }
                } else if (comp > 0) {
                    if (currentNode.getLeft() != null) {
                        currentNode = currentNode.getLeft();
                    } else {
                        currentNode.setLeft(new Node<>(e));
                        size++;
                        return true;
                    }
                }
            }
        } else {
            throw new UnsupportedOperationException();
        }
        return false;
    }


    @Override
    public boolean remove(Object o) {//TODO
        if (isEmpty()) {
            return false;
        }
        Node<T>[] array = getNodes(o);
        Node<T> prevToRemoveNode = array[0];
        Node<T> toRemoveNode = array[1];

        if (prevToRemoveNode == null || toRemoveNode == null) { // o not on tree
            return false;
        }

        int comp = comparator.compare(prevToRemoveNode.getElement(), (T) o);

        if (toRemoveNode.getRight() == null && toRemoveNode.getLeft() == null) { // Remove the last
            if (comp < 0) { //toRemoveNode is on the Right of the prev
                prevToRemoveNode.setRight(null);
            } else if (comp > 0) { //toRemoveNode is on the Left of the prev
                prevToRemoveNode.setLeft(null);
            } else if (size == 1 && comp == 0) {
                root = null;
            }
            size--;
            return true;
        }

        Node<T> currentNode = toRemoveNode;
        Node<T> prevToCurrentNode = toRemoveNode;
        int count = 0;
        if (currentNode.getRight() != null) { // Initialise cycle
            prevToCurrentNode = currentNode;
            currentNode = currentNode.getRight(); // One to the right
            while (currentNode.getLeft() != null) {
                count++;
                prevToCurrentNode = currentNode;
                currentNode = currentNode.getLeft();
            }

            if (count > 0) {
                if (currentNode.getRight() != null) {
                    prevToCurrentNode.setLeft(currentNode.getRight());
                } else {
                    prevToCurrentNode.setLeft(null);
                }

                currentNode.setLeft(toRemoveNode.getLeft());
                currentNode.setRight(toRemoveNode.getRight());


                if (comp < 0) { //toRemoveNode is on the Right of the prev
                    prevToRemoveNode.setRight(currentNode);
                } else if (comp > 0) { //toRemoveNode is on the Left of the prev
                    prevToRemoveNode.setLeft(currentNode);
                }
            } else {

                if (currentNode.getLeft() == null) {
                    prevToCurrentNode.setLeft(currentNode.getRight());
                }

                if (comp < 0) { //toRemoveNode is on the Right of the prev
                    prevToRemoveNode.setRight(currentNode);
                } else if (comp > 0) { //toRemoveNode is on the Left of the prev
                    prevToRemoveNode.setLeft(currentNode);
                }
            }


        }


        return false;
    }

    @Override
    public boolean containsAll(Collection<?> c) {
        for (Object o : c) {
            if (!contains(o)) {
                return false;
            }
        }
        return true;
    }

    @Override
    public boolean addAll(Collection<? extends T> c) {
        for (T e : c) {
            add(e);
        }
        return true;
    }

    @Override
    public boolean retainAll(Collection<?> c) {
        return false;
    }

    @Override
    public boolean removeAll(Collection<?> c) {
        for (Object e : c) {
            remove(e);
        }
        return true;
    }

    @Override
    public void clear() {
        root = null;
        size = 0;
    }


    // ONLY FOR TESTING
    /*
    public void print() {
        if (!isEmpty()) {
            printRec(root);
        } else {
            System.out.println("Empty");
        }
    }

    private void printRec(Node<T> node) {
        System.out.print("CURRENT: " + node.getElement() + " LEFT: ");
        if (node.getLeft() != null) {
            System.out.print(node.getLeft().getElement() + " RIGHT: ");
        } else {
            System.out.print("null RIGHT: ");
        }
        if (node.getRight() != null) {
            System.out.println(node.getRight().getElement());
        } else {
            System.out.println("null");
        }
        if (node.getLeft() != null) {
            printRec(node.getLeft());
        }
        if (node.getRight() != null) {
            printRec(node.getRight());
        }
    }*/
}
