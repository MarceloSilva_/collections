package com.marcelo.treeMap;

import java.util.*;

public class TreeMap<K, V> implements Map<K, V> {
    private Node<K, V> root;
    private Comparator<K> comparator;
    private int size;
    public Set<K> keys = new HashSet<>();

    TreeMap(Comparator<K> comparator) {
        this.comparator = comparator;
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public boolean isEmpty() {
        return size == 0;
    }

    @Override
    public boolean containsKey(Object key) {
        if (isEmpty()) {
            return false;
        }
        Node<K, V> currentNode = root;
        while (currentNode != null) {
            int comp = comparator.compare(currentNode.getKey(), (K) key);
            if (comp == 0) {
                return true;
            } else if (comp < 0) {
                if (currentNode.getRight() != null) {
                    currentNode = currentNode.getRight();
                } else {
                    return false;
                }
            } else if (comp > 0) {
                if (currentNode.getLeft() != null) {
                    currentNode = currentNode.getLeft();
                } else {
                    return false;
                }
            }
        }
        return false;
    }

    @Override
    public boolean containsValue(Object value) {
        return false;
    }

    @Override
    public V get(Object key) {
        if (isEmpty()) {
            return null;
        }
        Node<K, V> currentNode = root;
        while (currentNode != null) {
            int comp = comparator.compare(currentNode.getKey(), (K) key);
            if (comp == 0) {
                return currentNode.getValue();
            } else if (comp < 0) {
                if (currentNode.getRight() != null) {
                    currentNode = currentNode.getRight();
                } else {
                    return null;
                }
            } else if (comp > 0) {
                if (currentNode.getLeft() != null) {
                    currentNode = currentNode.getLeft();
                } else {
                    return null;
                }
            }
        }
        return null;
    }

    @Override
    public V put(K key, V value) {
        if (isEmpty()) {
            root = new Node<K, V>(key, value);
            keys.add(key);
            size++;
            return null;
        }
        Node<K, V> currentNode = root;
        if (comparator != null) {
            while (currentNode != null) {
                int comp = comparator.compare(currentNode.getKey(), (K) key);
                if (comp == 0) {
                    V temp = currentNode.getValue();
                    currentNode.setValue(value);
                    return temp;
                } else if (comp < 0) {
                    if (currentNode.getRight() != null) {
                        currentNode = currentNode.getRight();
                    } else {
                        currentNode.setRight(new Node<>(key, value));
                        keys.add(key);
                        size++;
                        return null;
                    }
                } else if (comp > 0) {
                    if (currentNode.getLeft() != null) {
                        currentNode = currentNode.getLeft();
                    } else {
                        currentNode.setLeft(new Node<>(key, value));
                        keys.add(key);
                        size++;
                        return null;
                    }
                }
            }
        } else {
            throw new UnsupportedOperationException();
        }
        return null;
    }

    @Override
    public V remove(Object key) {
        return null;
    }

    @Override
    public void putAll(Map<? extends K, ? extends V> m) {
    }

    @Override
    public void clear() {
        size = 0;
        root = null;
    }


    @Override
    public Set<K> keySet() {
        return keys;
    }

    @Override
    public Collection<V> values() {
        return null;
    }

    @Override
    public Set<Entry<K, V>> entrySet() {
        return null;
    }

}
