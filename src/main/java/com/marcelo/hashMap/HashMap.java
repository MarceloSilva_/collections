package com.marcelo.hashMap;

import java.util.Collection;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import static java.util.Objects.hash;

public class HashMap<K, V> implements Map<K, V> {
    private final int DEFAULT_BUCKETS = 16;
    private int n;
    private int size;
    private Node<K, V>[] buckets;
    private HashSet<K> keys;


    public HashMap() {
        n = DEFAULT_BUCKETS;
        buckets = new Node[n];
        keys = new HashSet<>();
    }

    public HashMap(int nBuckets) {
        n = nBuckets;
        buckets = new Node[n];
        keys = new HashSet<>();
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public boolean isEmpty() {
        return size == 0;
    }

    @Override
    public boolean containsKey(Object key) {
        return get(key) != null;
    }

    @Override
    public boolean containsValue(Object value) {
        return false;
    }

    @Override
    public V get(Object key) {
        int hash = hash(key);
        int index = calculateIndex(hash);
        //System.out.println(key + ": " + hash + " " + index);

        Node<K, V> current = buckets[index];

        if (current == null) {
            return null;
        } else {
            do {
                if (current.getHash() == hash) {
                    return current.getValue();
                }
                if (current.getNext() != null) {
                    current = current.getNext();
                } else {
                    break;
                }
            } while (current != null);
            return null;
        }
    }

    @Override
    public V put(K key, V value) {
        int hash = hash(key);
        int index = calculateIndex(hash);

        Node<K, V> current = buckets[index];
        if (current == null) {
            buckets[index] = new Node(hash, key, value, null);
            size++;
            //System.out.println("EMPTY :      Hash: " + hash + " Index: " + index + " RETURNIG: null");
            return null;
        } else {
            do {
                //System.out.println("DO :      Hash: " + hash + " Index: " + index + " KEY: " + key + " RETURNIG: " + current.getValue());

                if (current.getHash() == hash) {
                    V val = current.getValue();
                    current.setValue(value);
                    //System.out.println("REPETIDO");
                    return val;
                }
                if (current.getNext() != null) {
                    current = current.getNext();
                }
            } while (current.getNext() != null);
            current.setNext(new Node<K, V>(hash, key, value, null));
            keys.add(key);
            size++;
            //System.out.println("NOT_EMPTY:   Hash: " + hash + " Index: " + index + " RETURNIG: " + current.getValue());
            return current.getValue();
        }
    }

    private int calculateIndex(int hash) {
        return hash & (n - 1);
    }

    @Override
    public V remove(Object key) {
        int hash = hash(key);
        int index = calculateIndex(hash);

        Node<K, V> current = buckets[index];
        Node<K, V> prev = buckets[index];
        V removed = null;
        int counter = 0;
        if (current != null) {
            do {
                if (key != null) { //    Compare with .equal  - not null
                    if (key.equals(current.getKey())) {
                        if (counter == 0) {
                            buckets[index] = current.getNext();
                            size--;
                            return current.getValue();
                        } else {
                            prev.setNext(current.getNext());
                            size--;
                            return current.getValue();
                        }
                    }
                } else { // Compare with ==  - key == null
                    if (key == current.getKey()) {
                        if (counter == 0) {
                            buckets[index] = current.getNext();
                            size--;
                            return current.getValue();
                        } else {
                            prev.setNext(current.getNext());
                            size--;
                            return current.getValue();
                        }
                    }
                }
                prev = current;
                current = current.getNext();
                counter++;
            } while (current != null);
        }
        return removed;
    }

    @Override
    public void putAll(Map<? extends K, ? extends V> m) {
        for (Map.Entry entry : m.entrySet()) {
            put((K) entry.getKey(), (V) entry.getValue());
        }
    }

    @Override
    public void clear() {
        buckets = new Node[n];
        size = 0;
    }

    @Override
    public Set<K> keySet() { // TODO
        return keys;
    }

    @Override
    public Collection<V> values() {

        return null;
    }

    @Override
    public Set<Entry<K, V>> entrySet() { //TODO
        return null;
    }

}
