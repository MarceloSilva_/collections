package com.marcelo.linkedList;

public class Node<E> {
    private E element;
    private Node<E> next;


    public Node(E element, Node<? extends E> next) {
        this.element = element;
        this.next = (Node<E>) next;
    }

    public Node(E element) {
        this.element = element;
    }

    public Node<E> getNext() {
        return next;
    }

    public void setNext(Node<? extends E> next) {
        this.next = (Node<E>) next;
    }

    public E getElement() {
        return element;
    }

    public void setElement(E element) {
        this.element = element;
    }
}
