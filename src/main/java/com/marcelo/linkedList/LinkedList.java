package com.marcelo.linkedList;

import java.util.*;

public class LinkedList<E> implements List<E>, Deque<E> {
    private Node<E> head;
    private Node<E> tail;
    private int size;


    /**
     * Appends the specified element to the end of this list.
     */

    @Override
    public boolean add(E e) {
        if (size > 0) {
            addLast(e);
        } else {
            addFirst(e);
        }
        return true;
    }


    /**
     * Inserts the specified element at the specified position in this list.
     **/
    @Override
    public void add(int index, E element) {
        if (index < 0 || index >= size()) {
            throw new IndexOutOfBoundsException();
        }
        if (index == 0) {
            addFirst(element);
            return;
        }
        if (index == size) {
            addLast(element);
            return;
        }

        Node<E> nodeToBeInserted = new Node<E>(element);
        Node<E> temporaryNode = head;
        for (int i = 1; i < index; i++) {
            temporaryNode = temporaryNode.getNext();
        }
        nodeToBeInserted.setNext(temporaryNode.getNext());
        temporaryNode.setNext(nodeToBeInserted);
        size++;
    }


    /**
     * Appends all of the elements in the specified collection to the end of this list,
     * in the order that they are returned by the specified collection's iterator.
     **/

    @Override
    public boolean addAll(Collection<? extends E> c) {
        if (c == null) {
            throw new NullPointerException();
        }
        for (E e : c) {
            add(e);
        }
        return true;
    }


    /**
     * Inserts the specified element at the beginning of this list.
     */

    @Override
    public void addFirst(E e) {
        Node<E> toInsert = new Node<E>(e);
        if (head == null) {
            head = tail = toInsert;
        } else {
            toInsert.setNext(head);
            head = toInsert;
        }
        size++;
    }


    /**
     * Appends the specified element to the end of this list.
     */

    @Override
    public void addLast(E e) {
        Node<E> toInsert = new Node<E>(e);
        if (tail == null) {
            head = tail = toInsert;
        } else {
            tail.setNext(toInsert);
            tail = toInsert;
        }
        size++;
    }


    /**
     * Removes all of the elements from this list.
     */

    @Override
    public void clear() {
        head = tail = null;
        size = 0;
    }


    /**
     * Returns true if this list contains the specified element.
     */

    @Override
    public boolean contains(Object o) {
        Node<E> current = head;
        for (int i = 0; i < size; i++) {
            if (current.getElement().equals(o)) {
                return true;
            }
            current = current.getNext();
        }
        return false;
    }


    /**
     * Retrieves, but does not remove, the head (first element) of this list.
     */

    @Override
    public E element() {
        if (!isEmpty()) {
            return head.getElement();
        }
        throw new NoSuchElementException();
    }


    /**
     * Returns the element at the specified position in this list.
     */

    @Override
    public E get(int index) {
        if (index < 0 || index >= size()) {
            throw new IndexOutOfBoundsException();
        }

        Node<E> temporaryNode = head;
        int i = 0;

        while (temporaryNode != null && i < index) {
            temporaryNode = temporaryNode.getNext();
            i++;
        }
        return temporaryNode.getElement();
    }


    /**
     * Returns the first element in this list.
     */

    @Override
    public E getFirst() {
        return head.getElement();
    }


    /**
     * Returns the last element in this list.
     */

    @Override
    public E getLast() {
        return tail.getElement();
    }


    /**
     * Returns the index of the first occurrence of the specified element in this list,
     * or -1 if this list does not contain the element.
     */

    @Override
    public int indexOf(Object o) {
        Node<E> current = head;
        if (o == null) {
            for (int i = 0; i < size; i++) {
                if (current.getElement() == o) {
                    return i;
                }
                current = current.getNext();
            }
        } else {
            for (int i = 0; i < size; i++) {
                if (o.equals(current.getElement())) {
                    return i;
                }
                current = current.getNext();
            }
        }
        return -1;
    }


    /**
     * Returns the index of the last occurrence of the specified element in this list,
     * or -1 if this list does not contain the element.
     */

    @Override
    public int lastIndexOf(Object o) {
        Node<E> current = head;
        int foundAt = -1;
        if (o == null) {
            for (int i = 0; i < size; i++) {
                if (current.getElement() == o) {
                    foundAt = i;
                }
                current = current.getNext();
            }
        } else {
            for (int i = 0; i < size; i++) {
                if (o.equals(current.getElement())) {
                    foundAt = i;
                }
                current = current.getNext();
            }
        }
        return foundAt;
    }


    /**
     * Adds the specified element as the tail (last element) of this list.
     */

    @Override
    public boolean offer(E e) {
        addLast(e);
        return true;
    }


    /**
     * Inserts the specified element at the front of this list.
     */

    @Override
    public boolean offerFirst(E e) {
        addFirst(e);
        return true;
    }


    /**
     * Inserts the specified element at the end of this list.
     */

    @Override
    public boolean offerLast(E e) {
        addLast(e);
        return true;
    }


    /**
     * Retrieves, but does not remove, the head (first element) of this list.
     */

    @Override
    public E peek() {
        return peekFirst();
    }


    /**
     * Retrieves, but does not remove, the first element of this list, or returns null if this list is empty.
     */

    @Override
    public E peekFirst() {
        if (!isEmpty()) {
            return head.getElement();
        }
        return null;
    }


    /**
     * Retrieves, but does not remove, the last element of this list, or returns null if this list is empty.
     */

    @Override
    public E peekLast() {
        if (!isEmpty()) {
            return tail.getElement();
        }
        return null;
    }


    /**
     * Retrieves and removes the head (first element) of this list.
     */

    @Override
    public E poll() {
        return pollFirst();
    }


    /**
     * Retrieves and removes the first element of this list, or returns null if this list is empty.
     */

    @Override
    public E pollFirst() {
        if (!isEmpty()) {
            return removeFirst();
        }
        return null;
    }


    /**
     * Retrieves and removes the last element of this list, or returns null if this list is empty.
     */

    @Override
    public E pollLast() {
        if (!isEmpty()) {
            return removeLast();
        }
        return null;
    }


    /**
     * Pops an element from the stack represented by this list.
     */

    @Override
    public E pop() {
        if (!isEmpty()) {
            return removeFirst();
        }
        throw new NoSuchElementException();
    }


    /**
     * Pushes an element onto the stack represented by this list.
     */

    @Override
    public void push(E e) {
        addFirst(e);
    }


    /**
     * Retrieves and removes the head (first element) of this list.
     */

    @Override
    public E remove() {
        if (!isEmpty()) {
            return removeFirst();
        }
        throw new NoSuchElementException();
    }


    /**
     * Removes the element at the specified position in this list.
     */

    @Override
    public E remove(int index) {
        if (index < 0 || index >= size()) {
            throw new IndexOutOfBoundsException();
        }
        if (index == 0) {
            return removeFirst();
        }
        if (index == size - 1) {
            return removeLast();
        }
        Node<E> current = head;
        for (int i = 0; i < index - 1; i++) {
            current = current.getNext();
        }
        E elementRemoved = current.getNext().getElement();
        current.setNext(current.getNext().getNext());
        size--;
        return elementRemoved;
    }


    /**
     * Removes the first occurrence of the specified element from this list, if it is present.
     */

    @Override
    public boolean remove(Object o) {
        Node<E> current = head;
        Node<E> prev = head;
        for (int i = 0; i < size; i++) {
            if (current.getElement().equals(o)) {
                if (i == 0) {
                    removeFirst();
                    return true;
                } else if (i == size - 1) {
                    removeLast();
                    return true;
                } else {
                    prev.setNext(current.getNext());
                    size--;
                    return true;
                }
            }
            prev = current;
            current = current.getNext();
        }
        return false;
    }


    /**
     * Removes and returns the first element from this list.
     */

    @Override
    public E removeFirst() {
        E removed = head.getElement();
        head = head.getNext();
        size--;
        return removed;
    }


    /**
     * Removes the first occurrence of the specified element in this list (when traversing the list from head to tail).
     */

    @Override
    public boolean removeFirstOccurrence(Object o) {
        int index = indexOf(o);
        if (index != -1) {
            remove(index);
            return true;
        }
        return false;
    }


    /**
     * Removes and returns the last element from this list.
     */

    @Override
    public E removeLast() {
        Node<E> current = head;
        for (int i = 0; i < size - 1; i++) {
            current = current.getNext();
        }
        E removed = current.getElement();
        current.setNext(null);
        tail = current;
        size--;
        return removed;
    }


    /**
     * Removes the last occurrence of the specified element in this list (when traversing the list from head to tail).
     */

    @Override
    public boolean removeLastOccurrence(Object o) {
        int index = lastIndexOf(o);
        if (index != -1) {
            remove(index);
            return true;
        }
        return false;
    }


    /**
     * Replaces the element at the specified position in this list with the specified element.
     */

    @Override
    public E set(int index, E element) {
        if (index < 0 || index >= size()) {
            throw new IndexOutOfBoundsException();
        }
        Node<E> current = head;
        for (int i = 0; i < index; i++) {
            current = current.getNext();
        }
        E removedElement = current.getElement();
        current.setElement(element);
        return removedElement;
    }


    /**
     * Returns the number of elements in this list.
     */

    @Override
    public int size() {
        return size;
    }


    /**
     * Returns an array containing all of the elements in this list in proper sequence (from first to last element).
     */

    @Override
    public Object[] toArray() {
        E[] toReturn = (E[]) new Object[size()];
        Node<E> current = head;
        for (int i = 0; i < size; i++) {
            toReturn[i] = current.getElement();
            current = current.getNext();
        }
        return (E[]) toReturn;
    }


    /**
     * Removes all of this collection's elements that are also contained in the specified collection (optional operation).
     * After this call returns, this collection will contain no elements in common with the specified collection.
     */

    @Override
    public boolean removeAll(Collection<?> c) {
        for (Object o : c) {
            remove(o);
        }
        return true;
    }


    /**
     * Returns true if this list contains all of the elements of the specified collection.
     */

    @Override
    public boolean containsAll(Collection<?> c) {
        for (Object o : c) {
            if (!contains(o)) {
                return false;
            }
        }
        return true;
    }


    /**
     * Returns a view of the portion of this list between the specified fromIndex, inclusive, and toIndex, exclusive. (If fromIndex and toIndex are equal, the returned list is empty.)
     * The returned list is backed by this list, so non-structural changes in the returned list are reflected in this list, and vice-versa. The returned list supports all of the optional list operations supported by this list.
     */

    @Override
    public List<E> subList(int fromIndex, int toIndex) {
        if (fromIndex < 0 || toIndex > size) {
            throw new IndexOutOfBoundsException();
        }
        if (fromIndex > toIndex) {
            throw new IllegalArgumentException();
        }
        List<E> list = new LinkedList<E>();
        Node<E> current = head;
        for (int i = 0; i < toIndex; i++) {
            if (i >= fromIndex) {
                list.add(current.getElement());
            }
            current = current.getNext();
        }
        return list;
    }

    @Override
    public boolean isEmpty() {
        return size == 0;
    }

    // ##################

    @Override
    public Iterator<E> descendingIterator() {
        return null;
    }


    @Override
    public Iterator iterator() {
        Iterator<Object> it = new Iterator<Object>() {
            private Node<E> current = head;

            @Override
            public boolean hasNext() {
                return current != null;
            }

            @Override
            public Object next() {
                E data = current.getElement();
                current = current.getNext();
                return data;
            }
        };
        return it;
    }


    @Override
    public <T> T[] toArray(T[] a) {
        return null;
    }


    @Override
    public boolean addAll(int index, Collection<? extends E> c) {
        int i = index;
        for (E e : c) {
            add(i, e);
            i++;
        }
        return true;
    }


    @Override
    public boolean retainAll(Collection<?> c) {
        return false;
    }


    @Override
    public ListIterator<E> listIterator() {
        return null;
    }

    @Override
    public ListIterator<E> listIterator(int index) {
        return null;
    }
}
