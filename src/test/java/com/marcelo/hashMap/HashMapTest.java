package com.marcelo.hashMap;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class HashMapTest {
    HashMap<String, Integer> map;

    @Before
    public void setUp() {
        map = new HashMap<String, Integer>();
    }


    @Test
    public void sizeANDemptyANDclear() {
        assertTrue(map.isEmpty());
        assertEquals(0, map.size());
        map.put("OLA", 1);
        assertFalse(map.isEmpty());
        assertEquals(1, map.size());

        map.clear();
        assertTrue(map.isEmpty());
    }

    @Test
    public void putANDcontains() {
        assertFalse(map.containsKey("asdsdfsd"));
        assertEquals(null, map.put("asdsdfsd", 1));
        map.put("4gfdg", 1);
        map.put("gfuy9", 1);
        map.put("but47386ef", 1);
        map.put(null, 5);
        map.put("asdasd", 9);
        assertTrue(map.containsKey("asdasd"));
    }

    @Test
    public void remove() {
        map.put("asdsdfsd", 1);
        map.put("4gfdg", 4);
        map.put("gfuy9", 8);
        map.put("but47386ef", 3);
        map.put(null, 5);
        map.put("asdasd", 9);
        assertEquals((Integer) 8, map.remove("gfuy9"));
        assertEquals((Integer) 4, map.remove("4gfdg"));
        assertEquals((Integer) 5, map.remove(null));
    }


}