package com.marcelo.arrayList;

import org.junit.Before;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.*;

public class ArrayListTest {
    ArrayList<Integer> list;

    public void addForTesting(Integer[] arr) {
        for (Integer num : arr) {
            list.add(num);
        }
    }

    @Before
    public void setUp() throws Exception {
        list = new ArrayList<Integer>();
    }

    // size() : empty list

    @Test
    public void sizeZero() {
        assertEquals(0, list.size());
    }

    // size() : not empty list

    @Test
    public void sizeOne() {
        list.add(0);
        assertEquals(1, list.size());
    }

    //  isEmpty() : list is empty

    @Test
    public void isEmpty() {
        assertEquals(true, list.isEmpty());
    }

    // isEmpty(): list in not empty

    @Test
    public void isNotEmpty() {
        list.add(1);
        assertEquals(false, list.isEmpty());
    }

    // add() : testing add

    @Test
    public void add() {
        list.add(0);
        assertEquals((Integer) 0, list.get(0));
    }

    // add() : testing add after increasing Array size

    @Test
    public void addIncrementingSize() {
        list.add(0);
        list.add(1);
        list.add(2);
        list.add(3);
        list.add(4);
        list.add(5);
        list.add(6);
        list.add(7);
        list.add(8);
        list.add(9);
        list.add(10);
        assertEquals((Integer) 10, list.get(10));
    }


    // get() : testing the get method
    @Test
    public void get() {
        list.add(0);
        list.add(1);
        list.add(2);
        list.add(3);
        list.add(4);
        list.add(5);
        assertEquals((Integer) 5, list.get(5));
    }

    // get() : index out of bounds
    @Test(expected = IndexOutOfBoundsException.class)
    public void getExpectOutOfBounds() {
        list.add(0);
        list.add(1);
        list.add(2);
        list.add(3);
        list.add(4);
        list.add(5);
        list.get(10);
    }

    // toArray() : return an array
    @Test
    public void toArray() {
        list.add(0);
        list.add(1);
        list.add(2);
        Integer[] arr = new Integer[]{0, 1, 2};
        assertArrayEquals(arr, list.toArray());
    }

    // remove() : remove the last index
    @Test
    public void removeLast() {
        list.add(0);
        list.add(1);
        list.add(2);
        list.add(3);
        list.add(4);
        list.add(5);
        list.add(6);
        list.add(7);
        list.add(8);
        list.add(9);
        list.remove(9);
        Integer[] arr = new Integer[]{0, 1, 2, 3, 4, 5, 6, 7, 8};
        assertArrayEquals(arr, list.toArray());
    }

    // remove() : remove the first index
    @Test
    public void removeFirst() {
        list.add(0);
        list.add(1);
        list.add(2);
        list.add(3);
        list.add(4);
        list.add(5);
        list.add(6);
        list.add(7);
        list.add(8);
        list.add(9);
        list.remove(0);
        Integer[] arr = new Integer[]{1, 2, 3, 4, 5, 6, 7, 8, 9, null};
        assertEquals((Integer) 1, list.get(0));
    }

    // remove() : remove the middle index
    @Test
    public void removeMiddle() {
        list.add(0);
        list.add(1);
        list.add(2);
        list.add(3);
        list.add(4);
        list.add(5);
        list.add(6);
        list.add(7);
        list.add(8);
        list.add(9);
        list.remove(5);
        Integer[] arr = new Integer[]{0, 1, 2, 3, 4, 6, 7, 8, 9, null};
        assertEquals((Integer) 6, list.get(5));
    }

    // remove() : remove the middle index
    @Test(expected = IndexOutOfBoundsException.class)
    public void removeOutOfBounds() {
        list.add(0);
        list.add(1);
        list.add(2);
        list.add(3);
        list.remove(20);
    }

    // clear() : clears the Array
    @Test
    public void clear() {
        list.add(0);
        list.add(1);
        list.add(2);
        list.clear();
        assertTrue(list.isEmpty());
    }

    // indexOf() : return the first found
    @Test
    public void indexOf_Null() {
        list.add(1);
        list.add(2);
        list.add(3);
        list.add(4);
        list.add(5);
        assertEquals(5, list.indexOf(null)); // index: 5, is the first null element in the list
    }


    @Test
    public void indexOf() {
        list.add(1);
        list.add(2);
        list.add(3);
        list.add(4);
        list.add(5);
        assertEquals(3, list.indexOf(4));
    }

    @Test
    public void indexOfNotFound() {
        list.add(1);
        list.add(2);
        list.add(3);
        list.add(4);
        list.add(5);
        assertEquals(-1, list.indexOf(6));
    }

    @Test
    public void lastIndexOfNull() {
        list.add(1);
        list.add(88);
        list.add(9);
        list.add(17);
        list.add(17);
        list.add(9);
        list.add(17);
        list.add(27);
        list.add(1);
        assertEquals(9, list.lastIndexOf(null));
    }

    @Test
    public void lastIndexOf() {
        list.add(1);
        list.add(88);
        list.add(9);
        list.add(17);
        list.add(17);
        list.add(9);
        list.add(17);
        list.add(27);
        list.add(1);
        assertEquals(8, list.lastIndexOf(1));
        assertEquals(6, list.lastIndexOf(17));
    }

    @Test
    public void lastIndexOfNotFound() {
        list.add(1);
        list.add(2);
        list.add(3);
        list.add(4);
        list.add(5);
        assertEquals(-1, list.lastIndexOf(10));
    }

    @Test
    public void contains_null() {
        list.add(1);
        list.add(2);
        list.add(3);
        list.add(4);
        list.add(5);
        assertTrue(list.contains(null)); //Suppose to contain null
    }

    @Test
    public void containsObject() {
        list.add(1);
        list.add(2);
        list.add(3);
        list.add(4);
        list.add(5);
        assertTrue(list.contains(4)); //Suppose to contain 1
    }

    @Test
    public void notContains() {
        list.add(1);
        list.add(2);
        list.add(3);
        list.add(4);
        list.add(5);
        assertFalse(list.contains(10));
    }


    @Test
    public void removeAtStart() {
        list.add(0);
        list.add(1);
        list.add(2);
        list.add(3);
        list.add(4);
        list.add(5);
        assertTrue(list.remove(new Integer(0)));
    }


    @Test
    public void removeAtEnd() {
        list.add(0);
        list.add(1);
        list.add(2);
        list.add(3);
        list.add(4);
        list.add(5);
        list.add(6);
        assertTrue(list.remove(new Integer(6)));
    }

    @Test
    public void removeAtMiddle() {
        list.add(0);
        list.add(1);
        list.add(2);
        list.add(3);
        list.add(4);
        list.add(5);
        assertTrue(list.remove(new Integer(3)));
    }

    @Test
    public void removeNotFound() {
        list.add(0);
        list.add(1);
        list.add(2);
        list.add(3);
        list.add(4);
        list.add(5);
        assertFalse(list.remove(new Integer(10)));
    }

    @Test
    public void addAtIndexStart() {
        list.add(0);
        list.add(1);
        list.add(2);
        list.add(3);
        list.add(4);
        list.add(5);
        list.add(0, 10);
        assertEquals((Integer) 10, list.get(0));
    }

    @Test
    public void addAtIndexMiddle() {
        list.add(0);
        list.add(1);
        list.add(2);
        list.add(3);
        list.add(4);
        list.add(5);
        list.add(2, 10);
        assertEquals((Integer) 10, list.get(2));
    }

    @Test
    public void addAtIndexEnd() {
        list.add(0);
        list.add(1);
        list.add(2);
        list.add(3);
        list.add(4);
        list.add(5);
        list.add(5, 10);

        assertEquals((Integer) 10, list.get(5));
    }

    @Test(expected = IndexOutOfBoundsException.class)
    public void addAtIndexIndexOutOfBounds() {
        list.add(0);
        list.add(1);
        list.add(2);
        list.add(3);
        list.add(4);
        list.add(5);
        list.add(15, 10);
    }

    @Test
    public void addAtIndexIncresingSize() {
        list.add(0);
        list.add(1);
        list.add(2);
        list.add(3);
        list.add(4);
        list.add(5);
        list.add(6);
        list.add(7);
        list.add(8);
        list.add(9, 9);
    }

    @Test
    public void setAtStart() {
        list.add(0);
        list.add(1);
        list.add(2);
        list.add(3);
        list.add(4);
        list.add(5);
        list.set(0, 10);
        assertEquals((Integer) 10, list.get(0));
    }

    @Test
    public void setAtMiddle() {
        list.add(0);
        list.add(1);
        list.add(2);
        list.add(3);
        list.add(4);
        list.add(5);
        list.set(3, 10);
        assertEquals((Integer) 10, list.get(3));
    }

    @Test(expected = IndexOutOfBoundsException.class)
    public void setIndexOutOfBounds() {
        list.add(0);
        list.add(1);
        list.add(2);
        list.add(3);
        list.add(4);
        list.add(5);
        list.set(15, 10);
    }

    @Test
    public void addAll() {
        java.util.ArrayList<Integer> arrList = new java.util.ArrayList<Integer>() {{
            add(4);
            add(5);
            add(6);
        }};
        list.add(0);
        list.add(1);
        list.add(2);
        list.add(3);
        list.addAll(arrList);
        assertEquals((Integer) 6, list.get(6));
    }

    @Test(expected = NullPointerException.class)
    public void addAllNullPointer() {
        java.util.ArrayList<Integer> arrList = null;
        list.addAll(arrList);
    }

    @Test
    public void removeAll() {
        java.util.ArrayList<Integer> arrList = new java.util.ArrayList<Integer>() {{
            add(2);
            add(5);
            add(3);
        }};
        list.add(0);
        list.add(1);
        list.add(2);
        list.add(3);
        list.removeAll(arrList);
        Integer[] arr = new Integer[]{0, 1};
        assertArrayEquals(arr, list.toArray());
    }

    @Test(expected = NullPointerException.class)
    public void removeAllNullPointer() {
        java.util.ArrayList<Integer> arrList = null;
        list.removeAll(arrList);
    }

    @Test
    public void containsAllTrue() {
        java.util.ArrayList<Integer> arrList = new java.util.ArrayList<Integer>() {{
            add(2);
            add(5);
            add(3);
        }};
        list.add(0);
        list.add(1);
        list.add(2);
        list.add(3);
        list.add(4);
        list.add(5);
        list.add(6);
        assertTrue(list.containsAll(arrList));
    }

    @Test
    public void containsAllFalse() {
        java.util.ArrayList<Integer> arrList = new java.util.ArrayList<Integer>() {{
            add(2);
            add(5);
            add(3);
        }};
        list.add(0);
        list.add(1);
        list.add(3);
        list.add(4);
        list.add(5);
        list.add(6);
        assertFalse(list.containsAll(arrList));
    }

    @Test
    public void subList() {
        list.add(0);
        list.add(1);
        list.add(2);
        list.add(3);
        list.add(4);
        List<Integer> subList = list.subList(2, 4);
        Integer[] arr = new Integer[]{2, 3};
        assertArrayEquals(arr, subList.toArray());
    }

    @Test(expected = IndexOutOfBoundsException.class)
    public void subListIndexOutOfBounds() {
        list.add(0);
        list.add(1);
        list.add(2);
        list.add(3);
        list.add(4);
        list.subList(9, 10);
    }

    @Test(expected = IllegalArgumentException.class)
    public void subListIllegalArg() {
        list.add(0);
        list.add(1);
        list.add(2);
        list.add(3);
        list.add(4);
        list.subList(1, 0);
    }

    @Test
    public void addAllIndex() {
        java.util.ArrayList<Integer> arrList = new java.util.ArrayList<Integer>() {{
            add(2);
            add(3);
        }};
        list.add(0);
        list.add(1);
        list.add(4);
        list.add(5);
        list.add(6);
        list.addAll(2, arrList);
        Integer[] arr = new Integer[]{0, 1, 2, 3, 4, 5, 6};
        assertArrayEquals(arr, list.toArray());
    }

    @Test(expected = NullPointerException.class)
    public void addAllIndexNull(){
        list.addAll(1,null);
    }

    @Test(expected = IndexOutOfBoundsException.class)
    public void addAllIndexOutOfBounds(){
        java.util.ArrayList<Integer> arrList = new java.util.ArrayList<Integer>();
        list.add(0);
        list.add(1);
        list.add(2);
        list.addAll(50,arrList);
    }

    @Test
    public void iterator() {
        list.add(0);
        list.add(1);
        list.add(2);
        int i=0;
        for(Integer num : list){
            assertEquals(list.get(i),num);
            i++;
        }
    }
}