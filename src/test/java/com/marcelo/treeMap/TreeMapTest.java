package com.marcelo.treeMap;

import org.junit.Before;
import org.junit.Test;

import java.util.Comparator;

import static org.junit.Assert.*;

public class TreeMapTest {
    public TreeMap<Integer, String> tree;


    @Before
    public void setUp() {
        tree = new TreeMap<>(new IntComparator());
    }

    @Test
    public void clearSizeEmpty() {
        assertEquals(0, tree.size());
        assertTrue(tree.isEmpty());
        tree.put(1, "OLA");
        assertEquals(1, tree.size());
        assertFalse(tree.isEmpty());
        tree.clear();
        assertTrue(tree.isEmpty());
    }


    @Test
    public void put() {
        tree.put(10, "OLA1");
        tree.put(20, "OLA2");
        tree.put(15, "OLA3");
        tree.put(5, "OLA4");
        tree.put(6, "OLA5");
        assertEquals("OLA1", tree.put(10, "OLA6"));
    }


    @Test
    public void containsKey() {
        assertFalse(tree.containsKey(10));
        tree.put(10, "OLA1");
        tree.put(20, "OLA2");
        tree.put(15, "OLA3");
        tree.put(5, "OLA4");
        tree.put(6, "OLA5");
        assertEquals("OLA1", tree.put(10, "OLA6"));

        assertTrue(tree.containsKey(10));
        assertTrue(tree.containsKey(6));
        assertFalse(tree.containsKey(100));
    }

    @Test
    public void get() {
        assertEquals(null,tree.get(1));
        tree.put(10, "OLA1");
        tree.put(20, "OLA2");
        tree.put(15, "OLA3");
        tree.put(5, "OLA4");
        tree.put(6, "OLA5");
        assertEquals("OLA1", tree.put(10, "OLA6"));

        assertEquals("OLA5",tree.get(6));
        assertEquals("OLA6",tree.get(10));
        assertEquals(null,tree.get(100));
        assertEquals(null,tree.get(-100));
    }

    class IntComparator implements Comparator<Integer> {

        @Override
        public int compare(Integer o1, Integer o2) {
            return o1 - o2;
        }
    }
}