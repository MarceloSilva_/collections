package com.marcelo.hashSet;

import com.marcelo.linkedList.LinkedList;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class HashSetTest {
    HashSet<Integer> set;

    @Before
    public void setUp(){
        set= new HashSet<>();
    }

    @Test
    public void add(){
        assertTrue(set.add(1));
        assertTrue(set.add(2));
        assertTrue(set.add(3));
        assertTrue(set.add(4));
        assertTrue(set.add(5));
        assertTrue(set.add(6));
        assertTrue(set.add(7));
        assertTrue(set.add(8));
        assertTrue(set.add(9));
        assertTrue(set.add(null));
        assertTrue(set.add(10));
        assertTrue(set.add(11));
        assertTrue(set.add(12));
        assertTrue(set.add(13));
        assertTrue(set.add(14));
        assertTrue(set.add(15));
        assertTrue(set.add(16));
        assertTrue(set.add(17));
        assertTrue(set.add(18));
        assertTrue(set.add(19));
        assertTrue(set.add(20));
        assertFalse(set.add(1));
        assertFalse(set.add(19));
        //System.out.println(set.toString());
    }

    @Test
    public void remove(){
        set.add(1);
        set.add(2);
        set.add(3);
        set.add(4);
        set.add(5);
        set.add(6);
        set.add(7);
        set.add(8);
        set.add(9);
        set.add(10);
        set.add(11);
        set.add(12);
        set.add(13);
        set.add(14);
        set.add(15);
        set.add(16);
        set.add(17);
        set.add(18);
        set.add(19);
        set.add(20);
        set.add(21);
        set.add(null);

        assertTrue(set.remove(1));
        assertFalse(set.remove(1));
        assertTrue(set.remove(20));
        assertFalse(set.remove(20));
        assertTrue(set.remove(17));
        assertTrue(set.remove(null));
        assertTrue(set.remove(16));

        set.add(null);
        set.add(16);

        assertTrue(set.remove(null));

        //System.out.println(set.toString());
    }


    @Test
    public void size(){
        assertEquals(0,set.size());
        set.add(12);
        assertEquals(1,set.size());
    }

    @Test
    public void isEmpty(){
        assertTrue(set.isEmpty());
        set.add(12);
        assertFalse(set.isEmpty());
    }

    @Test
    public void contains() {
        set.add(1);
        assertTrue(set.contains(1));
        assertFalse(set.contains(null));
        set.add(2);
        set.add(3);
        set.add(4);
        set.add(5);
        set.add(6);
        set.add(7);
        set.add(8);
        set.add(9);
        set.add(10);
        set.add(11);
        set.add(12);
        set.add(13);
        set.add(14);
        set.add(15);
        set.add(16);
        set.add(17);
        set.add(18);
        set.add(19);
        set.add(20);
        set.add(21);
        set.add(null);

        assertTrue(set.contains(1));
        assertTrue(set.contains(null));
        assertTrue(set.contains(19));
    }

    @Test
    public void containsAll() {
        List<Integer> list = new LinkedList<>();
        list.add(1);
        assertFalse(set.containsAll(list));
        list.add(2);
        list.add(3);
        set.add(1);
        set.add(2);
        set.add(3);
        assertTrue(set.containsAll(list));
    }

    @Test
    public void addAll() {
        List<Integer> list = new LinkedList<>();
        list.add(1);
        list.add(2);
        list.add(3);
        assertTrue(set.addAll(list));
        assertTrue(set.containsAll(list));
    }

    @Test
    public void removeAll() {
        set.add(1);
        set.add(2);
        set.add(3);
        set.add(4);
        set.add(5);
        set.add(6);
        assertEquals(6,set.size());
        List<Integer> list = new LinkedList<>();
        list.add(1);
        list.add(2);
        list.add(3);
        list.add(10);
        assertTrue(set.removeAll(list));
        assertEquals(3,set.size());
    }

    @Test
    public void clear() {
        set.add(1);
        set.add(2);
        set.add(3);
        assertEquals(3,set.size());
        set.clear();
        assertEquals(0,set.size());
    }

}