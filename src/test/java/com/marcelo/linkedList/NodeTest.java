package com.marcelo.linkedList;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class NodeTest {
    Node<String> head;
    Node<String> two;
    Node<String> three;


    @Before
    public void setUp() {
        three = new Node<String>("three",null);
        two = new Node<String>("two",three);
        head = new Node<String>("one",two);

    }

    @Test
    public void getNext() {
        assertEquals(two,head.getNext());
    }

    @Test
    public void setNext() {
        head.setNext(three);
        assertEquals(three,head.getNext());
    }

    @Test
    public void getElement() {
        assertEquals("one",head.getElement());
    }

    @Test
    public void setElement() {
        head.setElement("asd");
        assertEquals("asd",head.getElement());
    }
}