package com.marcelo.linkedList;

import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

import static org.junit.Assert.*;

public class LinkedListTest {
    private LinkedList<String> list;

    @Before
    public void setUp() {
        list = new LinkedList<>();
    }

    @Test
    public void size() {
        assertEquals("Size must be 0", 0, list.size());
        list.addFirst("a");
        assertEquals("Size must be 1", 1, list.size());
    }

    @Test
    public void isEmpty() {
        assertTrue(list.isEmpty());
        list.addFirst("a");
        assertFalse(list.isEmpty());
    }

    @Test
    public void addFirst() {
        list.addFirst("zero");
        assertEquals("zero", list.getFirst());
    }

    @Test
    public void addFirstMultiples() {
        list.addFirst("zero");
        list.addFirst("one");
        list.addFirst("two");
        assertEquals("two", list.getFirst());
    }

    @Test
    public void addLast() {
        list.addLast("zero");
        assertEquals("zero", list.getLast());
    }

    @Test
    public void addLastMultiples() {
        list.addLast("zero");
        list.addLast("one");
        list.addLast("two");
        assertEquals("two", list.getLast());
    }

    @Test
    public void getFirst() {
        list.addLast("zero");
        list.addLast("one");
        list.addLast("two");
        list.addFirst("zero");
        list.addFirst("one");
        list.addFirst("two");
        assertEquals("two", list.getFirst());
    }

    @Test
    public void getLast() {
        list.addLast("zero");
        list.addLast("one");
        list.addLast("two");
        list.addLast("last");
        list.addFirst("zero");
        list.addFirst("one");
        list.addFirst("two");
        assertEquals("last", list.getLast());
    }

    @Test
    public void removeFirst() {
        list.addLast("zero");
        list.addLast("one");
        list.addLast("two");
        list.addLast("last");
        list.addFirst("zero");
        list.addFirst("one");
        list.addFirst("two");
        assertEquals("two", list.removeFirst());
    }

    @Test
    public void removeLast() {
        list.addLast("four");
        list.addLast("five");
        list.addLast("six");
        list.addLast("seven");
        list.addFirst("three");
        list.addFirst("two");
        list.addFirst("one");
        assertEquals("seven", list.removeLast());
    }

    @Test
    public void add() {
        list.add("one");
        assertEquals("one", list.getFirst());
        list.add("two");
        list.add("three");
        list.add("four");
        assertEquals("one", list.getFirst());
        assertEquals("four", list.getLast());
    }


    @Test
    public void toArray() {
        list.add("one");
        list.add("two");
        list.add("three");
        list.add("four");
        String[] arr = new String[]{"one", "two", "three", "four"};
        assertArrayEquals(arr, list.toArray());
        assertEquals(4, list.size());
    }

    @Test(expected = IndexOutOfBoundsException.class)
    public void addAtIndexOutOfBounds() {
        list.add(3, "one");
    }

    @Test
    public void addAt() {
        list.add("a");
        list.add(0, "zero");
        assertEquals("zero", list.getFirst());
        list.add(1,"oneInserted");
        assertEquals("oneInserted",list.get(1));
        list.add(2,"twoInserted");
        assertEquals("twoInserted",list.get(2));
    }

    @Test
    public void get() {
        list.add("zero");
        assertEquals("zero", list.get(0));
        list.add("one");
        list.add("two");
        assertEquals("two", list.get(2));
    }

    @Test(expected = IndexOutOfBoundsException.class)
    public void getOutOfBounds() {
        list.get(10);
    }

    //#########

    @Test(expected = IndexOutOfBoundsException.class)
    public void removeIndexOutOfBounds() {
        list.remove(10);
    }

    @Test
    public void removeAtIndexFirst() {
        list.add("zero");
        list.add("one");
        list.add("two");
        assertEquals("zero", list.remove(0));
    }

    @Test
    public void removeAtIndexLast() {
        list.add("zero");
        list.add("one");
        list.add("two");
        assertEquals("two", list.remove(2));
    }

    @Test
    public void removeAtIndexMiddle() {
        list.add("zero");
        list.add("one");
        list.add("two");
        list.add("three");
        list.add("four");
        assertEquals("two", list.remove(2));
        assertEquals("three", list.remove(2));
    }

    @Test
    public void remove() {
        list.add("zero");
        list.add("one");
        list.add("two");
        list.remove();
        assertEquals("one", list.get(0));
    }


    @Test(expected = NoSuchElementException.class)
    public void removeNoSuchElement() {
        list.remove();
    }

    @Test
    public void addAll() {
        java.util.ArrayList<String> arrList = new java.util.ArrayList<String>() {{
            add("four");
            add("five");
            add("six");
        }};
        list.add("zero");
        list.add("one");
        list.add("two");
        list.add("three");
        list.addAll(arrList);
        assertEquals("six", list.get(6));
    }

    @Test
    public void addAllAtIndex() {
        java.util.ArrayList<String> arrList = new java.util.ArrayList<String>() {{
            add("four");
            add("five");
            add("six");
        }};
        list.add("zero");
        list.add("one");
        list.add("two");
        list.add("three");
        list.addAll(1,arrList);
        assertEquals("four", list.get(1));
    }

    @Test(expected = NullPointerException.class)
    public void addAllNullPointer() {
        java.util.ArrayList<String> arrList = null;
        list.addAll(arrList);
    }


    @Test
    public void removeObjectOne() {
        list.add("zero");
        assertTrue(list.remove("zero"));
    }

    @Test
    public void removeObjectFirst() {
        list.add("zero");
        list.add("one");
        assertTrue(list.remove("zero"));
        assertEquals("one", list.get(0));
        assertEquals(1, list.size());
    }

    @Test
    public void removeObjectLast() {
        list.add("zero");
        list.add("one");
        list.add("two");
        list.add("three");
        assertTrue(list.remove("three"));
        assertEquals("two", list.get(2));
        assertEquals(3, list.size());
    }

    @Test
    public void removeObjectMiddle() {
        list.add("zero");
        list.add("one");
        list.add("two");
        list.add("three");
        list.add("four");
        assertTrue(list.remove("two"));
        assertEquals("three", list.get(2));
        assertEquals(4, list.size());
    }

    @Test
    public void removeObjectNotFound() {
        list.add("zero");
        list.add("one");
        list.add("two");
        list.add("three");
        list.add("four");
        assertFalse(list.remove("ten"));
        assertEquals(5, list.size());
    }

    @Test
    public void removeAll() {
        java.util.ArrayList<String> arrList = new java.util.ArrayList<String>() {{
            add("two");
            add("five");
            add("three");
        }};
        list.add("zero");
        list.add("one");
        list.add("two");
        list.add("three");
        list.removeAll(arrList);
        String[] arr = new String[]{"zero", "one"};
        assertArrayEquals(arr, list.toArray());
    }

    @Test
    public void element() {
        list.add("zero");
        assertEquals("zero", list.element());
    }

    @Test(expected = NoSuchElementException.class)
    public void elementNoSuchElement() {
        list.element();
    }

    @Test
    public void clear() {
        list.add("zero");
        list.add("one");
        list.add("two");
        list.add("three");
        list.clear();
        assertTrue(list.isEmpty());
    }

    @Test(expected = IndexOutOfBoundsException.class)
    public void setOutOfBounds() {
        list.set(10, "ten");
    }

    @Test
    public void setMiddle() {
        list.add("zero");
        list.add("one");
        list.add("two");
        list.add("three");
        assertEquals("one", list.set(1, "SetOne"));
        assertEquals("SetOne", list.get(1));
    }

    @Test
    public void setFirst() {
        list.add("zero");
        list.add("one");
        list.add("two");
        list.add("three");
        assertEquals("zero", list.set(0, "SetZero"));
        assertEquals("SetZero", list.get(0));
    }

    @Test
    public void setLast() {
        list.add("zero");
        list.add("one");
        list.add("two");
        list.add("three");
        assertEquals("three", list.set(3, "SetThree"));
        assertEquals("SetThree", list.get(3));
    }

    @Test
    public void contains() {
        list.add("zero");
        list.add("one");
        list.add("two");
        list.add("three");
        assertTrue(list.contains("two"));
        assertTrue(list.contains("three"));
        assertFalse(list.contains("ten"));
    }

    @Test
    public void indexOf() {
        list.add("zero");
        list.add("one");
        list.add("two");
        list.add("three");
        list.add("four");
        list.add(null);
        assertEquals(0, list.indexOf("zero"));
        assertEquals(2, list.indexOf("two"));
        assertEquals(4, list.indexOf("four"));
        assertEquals(-1, list.indexOf("ten"));
        assertEquals(5,list.indexOf(null));
    }


    @Test
    public void lastIndexOf() {
        list.add("zero");
        list.add("one");
        list.add("two");
        list.add("three");
        list.add("four");
        list.add("two");
        list.add("zero");
        list.add(null);
        assertEquals(6, list.lastIndexOf("zero"));
        assertEquals(5, list.lastIndexOf("two"));
        assertEquals(4, list.lastIndexOf("four"));
        assertEquals(-1, list.lastIndexOf("ten"));
        assertEquals(7,list.lastIndexOf(null));
    }

    @Test
    public void offer() {
        list.add("zero");
        list.add("one");
        list.add("two");
        list.add("three");
        list.add("four");
        list.offer("last");
        String[] arr = new String[]{"zero", "one", "two", "three", "four", "last"};
        assertEquals("Elements are not the same", "last", list.get(5));
        assertEquals("Wrong Size", 6, list.size());
        assertArrayEquals("Array not Equals", arr, list.toArray());

        list.clear();
        list.offer("add");
        String[] arr1 = new String[]{"add"};
        assertEquals("Elements are not the same", "add", list.get(0));
        assertEquals("Wrong Size", 1, list.size());
        assertArrayEquals("Array not Equals", arr1, list.toArray());
    }

    @Test
    public void offerFirst() {
        list.add("zero");
        list.add("one");
        list.add("two");
        list.add("three");
        list.add("four");
        list.offerFirst("first");
        String[] arr = new String[]{"first", "zero", "one", "two", "three", "four"};
        assertEquals("Elements are not the same", "first", list.get(0));
        assertEquals("Wrong Size", 6, list.size());
        assertArrayEquals("Array not Equals", arr, list.toArray());

        list.clear();
        list.offerFirst("add");
        String[] arr1 = new String[]{"add"};
        assertEquals("Elements are not the same", "add", list.get(0));
        assertEquals("Wrong Size", 1, list.size());
        assertArrayEquals("Array not Equals", arr1, list.toArray());
    }

    @Test
    public void offerLast() {
        list.add("zero");
        list.add("one");
        list.add("two");
        list.add("three");
        list.add("four");
        list.offerLast("last");
        String[] arr = new String[]{"zero", "one", "two", "three", "four", "last"};
        assertEquals("Elements are not the same", "last", list.get(5));
        assertEquals("Wrong Size", 6, list.size());
        assertArrayEquals("Array not Equals", arr, list.toArray());

        list.clear();
        list.offerLast("add");
        String[] arr1 = new String[]{"add"};
        assertEquals("Elements are not the same", "add", list.get(0));
        assertEquals("Wrong Size", 1, list.size());
        assertArrayEquals(arr1, list.toArray());
    }

    @Test
    public void peek() {
        list.add("zero");
        list.add("one");
        list.add("two");
        list.add("three");
        list.add("four");
        assertEquals("zero", list.peek());
        assertEquals("zero", list.peekFirst());
        assertEquals("four", list.peekLast());
        list.clear();
        assertEquals(null, list.peekFirst());
        assertEquals(null, list.peekLast());
    }

    @Test
    public void pool() {
        list.add("zero");
        list.add("one");
        list.add("two");
        list.add("three");
        list.add("four");
        assertEquals("zero", list.poll());
        assertEquals("one", list.pollFirst());
        assertEquals("four", list.pollLast());
        list.clear();
        assertEquals(null, list.pollFirst());
        assertEquals(null, list.pollLast());
    }

    @Test(expected = NoSuchElementException.class)
    public void popNoSuchElement() {
        list.pop();
    }

    @Test
    public void pop() {
        list.add("zero");
        list.add("one");
        list.add("two");
        list.add("three");
        list.add("four");
        assertEquals("zero", list.pop());
    }

    @Test
    public void push() {
        list.add("zero");
        list.add("one");
        list.add("two");
        list.add("three");
        list.add("four");
        list.push("five");
        assertEquals("five", list.get(0));
    }

    @Test
    public void removeFirstOccurrence() {
        list.add("zero");
        list.add("one");
        list.add("two");
        list.add("three");
        list.add("four");
        list.add("one");
        assertTrue(list.removeFirstOccurrence("one"));
        assertEquals("two", list.get(1));
    }

    @Test
    public void removeFirstOccurrenceNotFound() {
        list.add("zero");
        list.add("one");
        list.add("two");
        list.add("three");
        list.add("four");
        list.add("one");
        assertFalse(list.removeFirstOccurrence("ten"));
    }

    @Test
    public void removeLastOccurrence() {
        list.add("zero");
        list.add("one");
        list.add("two");
        list.add("three");
        list.add("one");
        list.add("four");
        assertTrue(list.removeLastOccurrence("one"));
        assertEquals("four", list.get(4));
    }

    @Test
    public void removeLastOccurrenceNotFound() {
        list.add("zero");
        list.add("one");
        list.add("two");
        list.add("three");
        list.add("four");
        list.add("one");
        assertFalse(list.removeLastOccurrence("ten"));
    }

    @Test
    public void containsAll() {
        ArrayList<String> arrList = new ArrayList<String>() {{
            add("one");
            add("four");
            add("zero");
        }};
        list.add("zero");
        list.add("one");
        list.add("two");
        list.add("three");
        list.add("four");
        assertTrue(list.containsAll(arrList));
        list.removeFirst();
        assertFalse(list.containsAll(arrList));
    }

    @Test
    public void subList() {
        list.add("zero");
        list.add("one");
        list.add("two");
        list.add("three");
        list.add("four");
        List<String> subList = list.subList(1, 4);
        String[] arr = new String[]{"one","two","three"};
        assertArrayEquals(arr,subList.toArray());
    }


    @Test(expected = IndexOutOfBoundsException.class)
    public void subListOutOfBounds() {
        list.subList(1,10);
    }


}
