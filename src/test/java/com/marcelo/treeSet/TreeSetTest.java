package com.marcelo.treeSet;

import org.junit.Before;
import org.junit.Test;

import java.util.Comparator;

import static org.junit.Assert.*;

public class TreeSetTest {
    TreeSet<Integer> tree;
    @Before
    public void setUp() {
        tree = new TreeSet<Integer>(new IntComparator());
    }


    @Test
    public void add() {
        tree.add(10);
        tree.add(20);
        tree.add(15);
        tree.size();
    }

    @Test
    public void clear() {
        assertTrue(tree.isEmpty());
        tree.add(10);
        assertFalse(tree.isEmpty());
        tree.clear();
        assertTrue(tree.isEmpty());
    }

    @Test
    public void contains() {
        assertFalse("Tree is suppose to be empty",tree.contains(10));
        assertTrue("Error adding '10'",tree.add(10));
        assertTrue("Error adding '20'",tree.add(20));
        assertTrue("Error adding '15'",tree.add(15));
        assertTrue("Entry '10' not found",tree.contains(10));
        assertFalse("Entry '2' is on the tree",tree.contains(2));
        assertTrue("Error adding '10'",tree.add(5));
        assertTrue("Error adding '6'",tree.add(6));
        assertTrue("Entry '10' not found",tree.contains(5));
        assertFalse("Entry '3' found",tree.contains(3));
        assertFalse("Entry '21' found",tree.contains(21));
        assertTrue("Entry '6' not found",tree.contains(6));
        assertFalse("Added duplicated entry",tree.add(10));
    }


    @Test
    public void remove() {
        tree.add(15);
        tree.add(1);
        tree.add(20);
        tree.remove(20);
    }

    @Test
    public void print() {
        tree.add(100);/*
        tree.add(200);
        tree.add(300);
        tree.add(400);
        tree.add(500);
        tree.add(50);
        tree.add(280);
        tree.add(270);
        tree.add(278);
        tree.add(60);
        tree.add(55);*/


        //tree.print();


        tree.remove(100);

        //tree.print();
    }

    class IntComparator implements Comparator<Integer> {

        @Override
        public int compare(Integer o1, Integer o2) {
            return o1-o2;
        }
    }
}


